
cubicFUSION Portlets
=====================================
* * *

http://www.portalzine.de

About
-----

Complete PHP / jQuery portlet system, build on top of jQuery UI / Sortable. Save position, save state, save visibility, allow settings per portlet and different setup of columns and portlets per page.


License
-----------

/LICENSE.txt.